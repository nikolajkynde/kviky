import pyudev
import subprocess
from subprocess import call
import os


def send2Pd(message=''):
        os.system("echo '" + message + "' | pdsend 3000")

context = pyudev.Context()
monitor = pyudev.Monitor.from_netlink(context)
monitor.filter_by(subsystem='usb')
done = 0
for device in iter(monitor.poll, None):
    print(device)
    call("sleep 3", shell=True)
    if os.path.exists("/media/usb/twerky.pd"): 
	    if device.action == 'add':
		#mount the drive
		# remove the present ext folder
		subprocess.check_call("sudo rm -r /home/pi/kviky/ext", shell=True)
		# make a new empty folder
		subprocess.check_call("sudo mkdir /home/pi/kviky/ext", shell=True)
		#call("sleep 3", shell=True)
		#print("usb plugged")
		subprocess.check_call("sudo cp -R /media/usb/* /home/pi/kviky/ext", shell=True)
    		if os.path.exists("/media/usb/*.zip"): 		
			subprocess.check_call('for file in /home/pi/kviky/ext/*.zip; do unzip -d "${file%.zip}" $file; done &', shell=True)
		#subprocess.check_call('sudo rm -r System\ Volume\ Information/', shell=True)
		print("files unziped")
		#subprocess.check_call("sudo killall pd &", shell=True)
		#print("PD killed")
		#call("sleep 3", shell=True)
		#call("pd -nogui $(ls -d /home/pi/ext/*/ | head -1)*.pd &", shell=True)
		call("sleep 1", shell=True)
		#print("pd started")
		subprocess.check_call("ls -d /home/pi/kviky/ext/*/*.pd | head > /home/pi/kviky/patchlist.txt", shell=True)
		subprocess.check_call("ls -d /home/pi/kviky/ext/*/ | head > /home/pi/kviky/pathlist.txt", shell=True)
		with open('/home/pi/kviky/patchlist.txt') as f:
      			content = f.readlines()
       			print(content[0])
        		send2Pd('3 ' + content[0] + ' ;')
		call("sudo pumount usb", shell=True)
		print("usb unmounted")
    else:
	    call("sudo pumount usb", shell=True)
	    print("usb unmounted")
