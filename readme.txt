Basic setup:
go to: https://www.raspberrypi.org/downloads/raspbian/
download Raspbian Stretch with desktop
follow these instructions
https://www.raspberrypi.org/documentation/installation/installing-images/README.md

connect screen, keyboard, mouse and Kviky to your computer and set up raspbery pi as prompted on the screen

go to terminal:...

### install Pure Data ###
sudo apt-get install pd


### clone software ####
git clone https://nikolajkynde@bitbucket.org/nikolajkynde/kviky.git


### Set up audio interface: ###
	
in /boot/config.txt comment out (put a # in front):
		write: sudo nano /boot/config.txt
		# dtparam=audio=on
	
and addon the line below:
		dtoverlay=hifiberry-dac
	
Create /etc/asound.conf with this content by writing:
		sudo nano /etc/asound.conf
and copy pasting this: 
		
	pcm.!default  {
 	 type hw card 0
	}
	ctl.!default {
	 type hw card 0
	}

## auto startup ###

Copy the following lines into /etc/rc.local before 'exit 0':

by writing:
		sudo nano /etc/rc.local

and copy pasting this:
		# run interface drivers
		sudo python /home/pi/kviky/TPinterfaceDrivers.py &
		# run stop button driver (the number three indicates what pin to run it on)
		/usr/local/bin/gpio-halt 3 &

##stop button##
clone from git:
		git clone https://github.com/adafruit/Adafruit-GPIO-Halt

	go to folder
		cd Adafruit-GPIO-Halt
	make it
		make
		sudo make install

### NOW REBOOT ###
