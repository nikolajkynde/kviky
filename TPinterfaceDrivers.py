#################### THIS IS THE DRIVERS FOR THE TWERKY PUNCHY PHYSICAL INTERFACE
####### it consists of five functions
######## init()initializes all values and start  the event detect for the knob and encoder
######## send2pd() send internal messages to Pure data
######## knob() take care of reading the ADC and sending aproriate messages to PD (channel 1)
######## buttonOn() is called at everytime the button is pressed and sends to PD (channel 0) and turn on and off the LED
######## rotation_decode() is called everytime the rotary encoder canges and sends to pd (channel 2)
### written by Nikolaj Kynde and Paul Versteeg


import RPi.GPIO as GPIO
from time import sleep
import time
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008
import os
import subprocess
import socket
import thread

#initialize some values
counter = 0
lastValue=1
ccLastValue=0


##### PIN SETUP ############
#ADC setup:
CLK  = 4
MISO = 14
MOSI = 15
CS   = 27
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)

#LED Setup
LED_pin=25
LED_pin2=26
LED_pinR=7
LED_pinG=8
LED_pinB=9

#button setup
time_stamp1 = time.time()
time_stamp2 = time.time()
time_stamp3 = time.time()
time_stamp4 = time.time()

pressed1 = 0
pressed2 = 0
pressed3 = 0
pressed4 = 0

btn_pin1 = 10
btn_pin2 = 17
btn_pin3 = 3
btn_pin4 = 2

# Rotary setup
Enc_A = 5  # Encoder input A:
Enc_B = 6  # Encoder input B:


############ THe Init functions runs ones the program starts ##########
def init():
    GPIO.setwarnings(True)

    # Use the Raspberry Pi BCM pins
    GPIO.setmode(GPIO.BCM)
    # BUTTON
    GPIO.setup(btn_pin1, GPIO.IN,pull_up_down=GPIO.PUD_UP) #use the build-in pull up resistor
    GPIO.setup(btn_pin2, GPIO.IN,pull_up_down=GPIO.PUD_UP) #use the build-in pull up resistor
    GPIO.setup(btn_pin3, GPIO.IN,pull_up_down=GPIO.PUD_UP) #use the build-in pull up resistor
    GPIO.setup(btn_pin4, GPIO.IN,pull_up_down=GPIO.PUD_UP) #use the build-in pull up resistor

    # LED
    GPIO.setup(LED_pin, GPIO.OUT) #set as output
    GPIO.setup(LED_pin2, GPIO.OUT) #set as output
    GPIO.setup(LED_pinR, GPIO.OUT) #set as output
    GPIO.setup(LED_pinG, GPIO.OUT) #set as output
    GPIO.setup(LED_pinB, GPIO.OUT) #set as output

    # define the Encoder switch inputs
    GPIO.setup(Enc_A, GPIO.IN, pull_up_down=GPIO.PUD_UP) # both are pull ups
    GPIO.setup(Enc_B, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    # setup an event detection thread for the A encoder switch
    GPIO.add_event_detect(Enc_A, GPIO.FALLING, callback=rotation_decode, bouncetime=2) # bouncetime in mSec
    GPIO.add_event_detect(btn_pin1, GPIO.FALLING, callback=buttonOn1, bouncetime=10)
    GPIO.add_event_detect(btn_pin2, GPIO.FALLING, callback=buttonOn1, bouncetime=10)
    GPIO.add_event_detect(btn_pin3, GPIO.FALLING, callback=buttonOn1, bouncetime=10)
    GPIO.add_event_detect(btn_pin4, GPIO.FALLING, callback=buttonOn1, bouncetime=10)
    #start driveopen.py
    subprocess.call("sudo python /home/pi/kviky/driveopen.py &", shell=True)    
    # start the  PDreceive listener in a seperate thread:
    thread.start_new_thread(pdreceive,(3001, ))
    # start the pd patch called midiTest.pd
    subprocess.call("pd -nogui -path /home/pi/kviky/ext/01lazers /home/pi/kviky/main1.pd &", shell=True)
    sleep(1)

#    with open('/home/pi/pathlist.txt') as f:
#    	content = f.readlines()
#	print(content[0])
#	subprocess.call('pd -path /home/pi/ext/01lazers &')

    with open('/home/pi/kviky/patchlist.txt') as f:
    	content = f.readlines()
	print(content[0])
	send2Pd('3 ' + content[0] + ' ;')
    return

##### A METHOD FOR RECIEVING DATA FROM PD #########
def pdreceive(port):
    # open listener socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost', port))
    s.listen(1)

    while True:       
        conn, addr = s.accept()
        for line in conn.makefile():
	    # if input = 1 turnon LED
            if line[6] == '1':
                    GPIO.output(LED_pin, GPIO.HIGH)
	    if line[6] == '0':
                    GPIO.output(LED_pin, GPIO.LOW)
	    if line[6] == '3': 
                    GPIO.output(LED_pin2, GPIO.HIGH)
            if line[6] == '2':
                    GPIO.output(LED_pin2, GPIO.LOW)
            if line[6] == '5':
                    GPIO.output(LED_pinR, GPIO.HIGH)
            if line[6] == '4':
                    GPIO.output(LED_pinR, GPIO.LOW)
            if line[6] == '7':
                    GPIO.output(LED_pinG, GPIO.HIGH)
            if line[6] == '6':
                    GPIO.output(LED_pinG, GPIO.LOW)
            if line[6] == '9':
                    GPIO.output(LED_pinB, GPIO.HIGH)
            if line[6] == '8':
                    GPIO.output(LED_pinB, GPIO.LOW)
        conn.close()
    sock.close()


###### A METHOD FOR COMUNICATING WITH PD #####
def send2Pd(message=''):
        os.system("echo '" + message + "' | pdsend 3000")
#an example of a pd message
#send2Pd('0 1;')
##################### all business with the KNOB #########################

def knob(MISO):
    global ccLastValue
    # The read_adc function will get the value of the specified channel (0-7).
    ccValue = mcp.read_adc(0)
#    print(ccValue)
    #Send  the ADC values to PD only if they are different from the last value.
    if(ccValue != ccLastValue):
       send2Pd('1 '+ str(ccValue) + ' ;')
    ccLastValue=ccValue


################## all bussiness with the BUTTON #########################
def buttonOn1(btn_pin):
	thread.start_new_thread(buttonOn, (btn_pin,))


def buttonOn(btn_pin):
	global pressed1
	global pressed2
	global pressed3
	global pressed4
#	print("callback started")
        time_now = time.time()  # make a time stamp of present time

        if (time_now - getTimeStamp(btn_pin)) >= 0.02: #if button was pressed less than 10 ms ago ignore the press
		#sleep(0.002) # wait for two miliseconds
		if(GPIO.input(btn_pin)== 0): #if the button state has changed within those miliseconds, it was propably not a real press, and it is ignored
			if(btn_pin == btn_pin1):
				if (pressed1==0):
					pressed1 = 1
					send2Pd('0 1;') # send the message 0 1 to pd
					while(GPIO.input(btn_pin) == 0): #wait untill the button is not on(0) anymore
						sleep(0.01)
					send2Pd('0 0;') # then send 0 0 to PD
					pressed1 = 0
		        if(btn_pin == btn_pin2):
				if (pressed2==0):
					pressed2 = 1
                                	send2Pd('4 1;') # send the message 1 1 to pd
                                	while(GPIO.input(btn_pin) == 0): #wait untill the button is not on(0) anymore
                                        	sleep(0.01)
                                	send2Pd('4 0;') # then send 1 0 to PD
					pressed2 = 0

                        if(btn_pin == btn_pin3):
                                if (pressed3==0):
                                        pressed3 = 1
                                        send2Pd('6 1;') # send the message 6 1 to pd
                                        while(GPIO.input(btn_pin) == 0): #wait untill the button is not on(0) anymore
                                                sleep(0.01)
                                        send2Pd('6 0;') # then send 6 0 to PD
                                        pressed3 = 0

                        if(btn_pin == btn_pin4):
				if (pressed4==0):
					pressed4 = 1
	                                send2Pd('5 1;') # send the message 5 1 to pd
        	                        while(GPIO.input(btn_pin) == 0): #wait untill the button is not on(0) anymore
                	                        sleep(0.01)
                        	        send2Pd('5 0;') # then send 5 0 to PD
					pressed4 = 0

#	else:
#		print("too fast")
        setTimeStamp(btn_pin)   # set the time stamp to present time  for comparison

def getTimeStamp(btn_pin):
	if(btn_pin==btn_pin1):
		return time_stamp1
	if(btn_pin==btn_pin2):
		return time_stamp2
	if(btn_pin==btn_pin4):
		return time_stamp4

def setTimeStamp(btn_pin):
	time_now = time.time()
	if(btn_pin==btn_pin1):
		global time_stamp1
		time_stamp1=time_now
        if(btn_pin==btn_pin2):
                global time_stamp2
                time_stamp2=time_now
        if(btn_pin==btn_pin4):
                global time_stamp4
                time_stamp4=time_now

################### all bussiness with the ENDLESS ENCODER #################
def rotation_decode(Enc_A):

    global counter
    # sleep(0.002) # extra 2 mSec de-bounce time

    # read both of the switches
    Switch_A = GPIO.input(Enc_A)
    Switch_B = GPIO.input(Enc_B)

    if (Switch_A == 0) and (Switch_B == 0) : # A then B ->
        counter += 1
        #print "direction -> ", counter
        # wait for A to go high again
        while Switch_A == 0:
            Switch_A = GPIO.input(Enc_A)
	send2Pd('2 '+ str(counter) + ' ;')
        return

    elif (Switch_A == 0) and (Switch_B == 1): # B then A <-
        counter -= 1
        #print "direction <- ", counter
         # wait for A to go high again
        while Switch_A == 0:
            Switch_A = GPIO.input(Enc_A)
        send2Pd('2 '+ str(counter) + ' ;')
	return

    else: # discard all other combinations
   	return


################## MAIN LOOP ########################################
def main():
    try:
	#start the init function
        init()
        while True :
           # this is a loop to avoid the program from ending
	    knob(MISO) #here we read the knob
	    sleep(0.1) #and wait for 10 ms
    except KeyboardInterrupt: # Ctrl-C to terminate the program
        GPIO.cleanup() # unassign all GPIO's

if __name__ == '__main__':
    main()
